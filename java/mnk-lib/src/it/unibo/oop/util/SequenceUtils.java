package it.unibo.oop.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SequenceUtils {

    public static <T> List<Tuple3<Integer, Integer, T>> subsequences(Stream<T> stream) {
    	final List<T> sequence = stream.collect(Collectors.toList());
        final List<Tuple3<Integer, Integer, T>> result = new LinkedList<>();
        
        int i = 0;
        T symbol = null;
        int index = -1;
        int length = 0;
        
        for (T e : sequence) {
        	if (Objects.equals(e, symbol)) {
                length++;
            } else {
                if (symbol != null) {
                    result.add(Tuple.of(index, length, symbol));
                }
                symbol = e;
                index = i;
                length = 1;
            }
            i++;
        }
        
        if (symbol != null) {
            result.add(Tuple.of(index, length, symbol));
        }
        return result;
    }
}
