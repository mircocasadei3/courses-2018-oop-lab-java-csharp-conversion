package it.unibo.oop.util;

@FunctionalInterface
public interface BiIntObjFunction<T, R> {
    R apply(int i, int j, T obj);
}
